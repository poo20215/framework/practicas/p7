<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Alumnos;
use app\models\Formulario;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionInsertarregistros()
    {
        $model = new \app\models\Alumnos();
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->save()) 
            {
                return $this->redirect(["site/index"]);
                
            }
        }
   
        return $this->render('formulario_insertar', [
            'model' => $model,
            'accion' => "Insertar"
        ]);
    }
    
    public function actionModificarregistros($codigoAlumno)
    {
        $model = Alumnos::findOne($codigoAlumno);
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->save()) 
            {
                return $this->redirect(["site/index"]);
                
            }
        }
   
        return $this->render('formulario_insertar', [
            'model' => $model,
            "accion" => "Modificar"
        ]);
    }
    
    public function actionEliminarregistros($codigoAlumno)
    {
        $model = Alumnos::findOne($codigoAlumno);
        
        if($this->request->isPost)
        {
            $model->delete();
            return $this->redirect(["site/index"]);
        }
        
        return $this->render('formulario_insertar', [
            'model' => $model,
            "accion" => "Eliminar"
        ]);
        
    }
    
    public function actionSeleccionaralumno($accion)
    {
        $model = new Formulario();
              
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate())
            {
                if($accion=="modificar")
                {
                    return $this->redirect(["site/modificarregistros","codigoAlumno"=>$model->codigoAlumno]);
                }else
                {
                    return $this->redirect(["site/eliminarregistros","codigoAlumno"=>$model->codigoAlumno]);
                }
                
            }
        }

        $alumnos= Alumnos::find()->all();
            $listar_alumnos= ArrayHelper::map($alumnos,'codigoAlumno',function($model)
            {
                return $model->nombre . " " . $model->apellidos; 
            }
            );
            
            return $this->render('formulario_codigo_alumno', [
                'model' => $model,
                'listar_alumnos' => $listar_alumnos,
            ]);
    }
    
    public function actionListarregistros()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Alumnos::find()]);
        return $this->render("listarregistros",["dataProvider" => $dataProvider]);
    }
    
    public function actionListarregistros2()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Alumnos::find()]);
        return $this->render("listarregistros2",["dataProvider" => $dataProvider]);
    }
    
}
