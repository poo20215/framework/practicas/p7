<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */
/* @var $form ActiveForm */
?>
<div class="formulario_insertar">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'nombre') ?>
        <?= $form->field($model, 'apellidos') ?>
        <?= $form->field($model, 'correo') ?>
        <?= $form->field($model, 'telefono') ?>
    
        <div class="form-group">
            <?= Html::submitButton($accion, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- formulario_insertar -->
