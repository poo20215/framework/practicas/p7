<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumnos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumnos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alumnos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoAlumno',
            'nombre',
            'apellidos',
            'telefono',
            'correo',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',  //orden de los botones    
                'buttons' => [
                'view' => function($url,$model){
                return Html::a('<i class="fas fa-eye"></i>',['alumnos/view','codigoAlumno' => $model->codigoAlumno]);
                },
                'update' => function($url,$model){
                    return Html::a('<i class="fas fa-pencil-alt"></i>',['alumnos/update','codigoAlumno' => $model->codigoAlumno]);
                    },
                'delete' => function($url,$model){
                    return Html::a('<i class="fas fa-trash-alt"></i>',['alumnos/delete','codigoAlumno' => $model->codigoAlumno],
                            ['data' => ['confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post']]);
                    },           
                            
            ]],
        ],
    ]); ?>


</div>
